package final1.algorithm;

/**
 * A checked Game exception.
 * Will be thrown, if the AIPlayer cannot calculate the next move.
 * 
 * @author Henning Dieterichs
 * @version 1.0
 */
public class GameException extends Exception {
    /**
     * Creates a new game exception.
     * @param message the message used to describe the exception.
     */
    public GameException(String message) {
        super(message);
    }
}
