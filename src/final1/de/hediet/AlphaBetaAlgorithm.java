package final1.de.hediet;

import final1.algorithm.AIPlayer;
import final1.algorithm.Game;
import final1.algorithm.GameException;
import final1.algorithm.GameState;
import final1.algorithm.Move;

/**
 * The Alpha Beta Algorithm which can be used for any two-player zero-sum game.
 *
 * @author Henning Dieterichs
 * @version 1.0
 *
 * @param <G> the type of game the algorithm uses.
 * @param <M> the type of move the game uses.
 */
public class AlphaBetaAlgorithm<G extends Game<M>, M extends Move> implements AIPlayer<M> {

    /**
     * Value of epsilon is taken from exercise sheet.
     */    
    private static final double EPSILON = 1E-14;
    
    private final G game;
    private final int depth;

    /**
     * Creates an new alpha beta algorithm.
     *
     * @param game the game used to calculate the next move. Cannot be null.
     * @param depth the depth, which determines how many moves in the future are
     * considered. Has to be greater than or equal to 1.
     */
    public AlphaBetaAlgorithm(G game, int depth) {
        ArgumentExceptionHelper.ensureNotNull(game, "game");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(depth, 1, "depth");

        this.game = game;
        this.depth = depth;
    }

    /**
     * Calculates and returns the estimated best move for the active AI-player.
     *
     * @return the estimated best move for the active player. Is never null.
     * @throws GameException when no move is possible.
     */
    @Override
    public M getBestMove() throws GameException {
        if (game.getState() != GameState.PLAYING) {
            throw new GameException("No move is possible.");
        }

        MoveContainer bestMoveContainer = new MoveContainer();
        max(depth, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, bestMoveContainer);

        assert bestMoveContainer.getMove() != null;
        return bestMoveContainer.getMove();
    }

    /**
     * The core of the alpha-beta algorithm. Algorithm taken from the exercise sheet.
     */
    private double max(int depth, double alpha, double beta, MoveContainer bestMoveContainer) {

        if (game.getState() != GameState.PLAYING || depth == 0) {
            return game.evaluateState();
        }

        double bestAlpha = alpha;

        M bestMove = null;

        for (M move : game.getValidMoves()) {
            if (bestMove == null) {
                bestMove = move;
            }

            game.perform(move);

            double value = -max(depth - 1, -beta, -bestAlpha, null);

            game.undo();

            if (greaterThan(value, bestAlpha)) {
                bestAlpha = value;
                bestMove = move;
                if (greaterThanOrEqual(value, beta)) {
                    break;
                }
            }
        }

        if (bestMoveContainer != null) {
            bestMoveContainer.setMove(bestMove);
        }

        return bestAlpha;
    }

    /**
     * Code taken from ilias.
     */
    private static boolean greaterThan(double a, double b) {
        if (a == b) { // in case of infinity
            return false;
        }
        return a - b > -EPSILON && !(Math.abs(a - b) < EPSILON);
    }

    /**
     * Code taken from ilias.
     */
    private static boolean greaterThanOrEqual(double a, double b) {
        if (a == b) { // in case of infinity
            return true;
        }
        return a - b > -EPSILON;
    }

    /**
     * The class is used to return the best move from {@link #max}.
     */
    private class MoveContainer {

        private M move;

        public MoveContainer() {
        }

        /**
         * Gets the move. Is only null, if {@link #setMove} was not called.
         *
         * @return the move or null.
         */
        public M getMove() {
            return move;
        }

        /**
         * Sets the move.
         *
         * @param value the move. Cannot be null.
         */
        public void setMove(M value) {
            ArgumentExceptionHelper.ensureNotNull(value, "value");
            move = value;
        }
    }
}
