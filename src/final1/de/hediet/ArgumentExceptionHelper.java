package final1.de.hediet;

/**
 * An helper class for argument exceptions.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public final class ArgumentExceptionHelper {

    private ArgumentExceptionHelper() {
        throw new AssertionError("Utility Classes must not be instantiated!");
    }

    /**
     * Ensures that <code>argument</code> is not null and throws an
     * {@link IllegalArgumentException} if it is null.
     *
     * @param argument the argument to check.
     * @param argumentName the argument name which will be used within the
     * exception.
     */
    public static void ensureNotNull(Object argument, String argumentName) {
        if (argument == null) {
            throw new IllegalArgumentException(
                    String.format("Argument %s cannot be null.", argumentName));
        }
    }

    /**
     * Ensures that <code>argument</code> is greater than or equal to
     * <code>greaterThanOrEqualTo</code> and throws an
     * {@link IllegalArgumentException} if it is not.
     *
     * @param argument the argument to check.
     * @param greaterThanOrEqualTo the value to compare against.
     * @param argumentName the argument name which will be used within the
     * exception.
     */
    public static void ensureGreaterThanOrEqualTo(int argument,
            int greaterThanOrEqualTo, String argumentName) {
        if (argument < greaterThanOrEqualTo) {
            throw new IllegalArgumentException(
                    String.format("Argument %s has to be greater than or equal to %d, but was %d.",
                            argumentName, greaterThanOrEqualTo, argument));
        }
    }

    /**
     * Ensures that <code>argument</code> is in the closed interval
     * [<code>min</code>, <code>max</code>] and throws an
     * {@link IllegalArgumentException} if it is not.
     *
     * @param argument the argument to check.
     * @param min the lower endpoint, inclusive.
     * @param max the upper endpoint, inclusive.
     * @param argumentName the argument name which will be used within the
     * exception.
     */
    public static void ensureWithinRange(int argument, int min, int max,
            String argumentName) {
        if (argument < min || argument > max) {
            throw new IllegalArgumentException(
                    String.format("Argument %s has to be in [%d, %d], but was %d.",
                            argumentName, min, max, argument));
        }
    }
}
