package final1.de.hediet;

import final1.Terminal;
import final1.algorithm.GameState;

/**
 * The ConnetFour Program. This class contains the entry point and initializes
 * the game with constants taken from the exercise sheet.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public final class ConnectFour {

    /**
     * The prefix which must be prepended to error outputs.
     */
    public static final String ERROR_PREFIX = "Error, ";
    private static final String PLAYER1_NAME = "Player 1";
    private static final String PLAYER2_NAME = "Player 2";
    private static final int FIELD_WIDTH = 7;
    private static final int FIELD_HEIGHT = 6;
    /**
     * The search depth the {@link ConnectFourPlayerTerminalComputer} instances will be initialized with.
     */
    private static final int AI_MOVE_DEPTH = 11;

    private ConnectFour() {
    }

    /**
     * The entry point.
     *
     * @param args the command line arguments. Should contain only the number of humans
     * involved in the game which can be either "0","1" or "2". Missing
     * participants are filled with artificial intelligence.
     */
    public static void main(String[] args) {
        try {

            ConnectFourGame game = initializeGame(args);
            playGame(game);

        } catch (IllegalArgumentException e) {
            //Catch only argument exceptions for argument "args".
            if (!e.getMessage().contains("Argument args")) {
                throw e;
            }

            String message = e.getMessage();
            //prettify argument exception
            message = message.replaceFirst("Argument args\\[0\\]", "Command line Argument");
            message = message.replaceFirst("Argument args", "Command line Arguments");

            Terminal.println(ERROR_PREFIX + message);
        }
    }

    /**
     * Initializes a new connect four game.
     *
     * @param args the arguments. Has to contain only the number of humans
     * involved in the game which can be either "0","1" or "2". Missing
     * participants are filled with artificial intelligence.
     * @return the initialized game.
     */
    private static ConnectFourGame initializeGame(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException(String.format(
                    "Argument args was expected to contain exactly 1 element, but got %d.", args.length));
        }

        int humanPlayers;
        try {
            humanPlayers = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(String.format(
                    "Argument args[0] has to be a valid number, but was '%s'.", args[0]), e);
        }

        ArgumentExceptionHelper.ensureWithinRange(humanPlayers, 0, 2, "args[0]");

        ConnectFourPlayer player1 = humanPlayers > 0
                ? new ConnectFourPlayerTerminalHuman(PLAYER1_NAME)
                : new ConnectFourPlayerTerminalComputer(PLAYER1_NAME, AI_MOVE_DEPTH);

        ConnectFourPlayer player2 = humanPlayers > 1
                ? new ConnectFourPlayerTerminalHuman(PLAYER2_NAME)
                : new ConnectFourPlayerTerminalComputer(PLAYER2_NAME, AI_MOVE_DEPTH);

        ConnectFourGame game = new ConnectFourGame(
                player1, player2, new ConnectFourField(FIELD_WIDTH, FIELD_HEIGHT));

        //The players need to know the game before they can play.
        player1.setGame(game);
        player2.setGame(game);

        return game;
    }

    /**
     * Plays the game.
     *
     * @param game the game to play. Cannot be null.
     */
    private static void playGame(ConnectFourGame game) {
        assert game != null;

        while (game.getState() == GameState.PLAYING) {
            Terminal.println(game.getFieldString());
            Terminal.println("");
            ConnectFourMove move = game.nextTurn();
            game.perform(move);
        }
        Terminal.println(game.getFieldString());

        GameState state = game.getState();

        if (state == GameState.PLAYER_1_WON) {
            Terminal.println(game.getPlayerByPosition(1).getName() + " won the game!");
        } else if (state == GameState.PLAYER_2_WON) {
            Terminal.println(game.getPlayerByPosition(2).getName() + " won the game!");
        } else if (state == GameState.DRAW) {
            Terminal.println("Draw!");
        }
    }
}
