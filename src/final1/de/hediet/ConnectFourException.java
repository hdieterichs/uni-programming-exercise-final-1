package final1.de.hediet;

/**
 * This exception will be thrown, if rules of the connect four game will be
 * violated. This exception is unchecked, because the Game interface does not
 * allow throwing checked exception.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ConnectFourException extends RuntimeException {
    /** Constructs a new connect four exception with <code>message</code> as its
     * detail message.
     * @param message the detail message.
     */
    public ConnectFourException(String message) {
        super(message);
    }
    
    /** Constructs a new connect four exception with <code>message</code> as its
     * detail message and <code>cause</code> as the exception which caused this exception.
     * @param message the detail message.
     * @param cause the exception which caused this exception.
     */
    public ConnectFourException(String message, Throwable cause) {
        super(message, cause);
    }

}
