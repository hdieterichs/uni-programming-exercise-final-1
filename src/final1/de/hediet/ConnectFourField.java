package final1.de.hediet;

/**
 * The field used in connect four. It ensures that coins fall down to bottom and
 * only topmost coins can be removed.
 *
 * <code>getState(0, 0)</code> gets the state of the bottom left field,
 * <code>getState(getWidth() - 1, getHeight() - 1)</code> gets the state of the
 * top right field.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ConnectFourField {

    private final ConnectFourPlayer[][] field;
    /**
     * Used to decide how many rows are taken in a specific column in O(1).
     */
    private final int[] takenRows;
    private final int height;

    /**
     * Creates a new connect four field.
     *
     * @param width the width of the field. Has to be greater than or equal to
     * 1.
     * @param height the height of the field. Has to be greater than or equal to
     * 1.
     */
    public ConnectFourField(int width, int height) {
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(width, 1, "width");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(height, 1, "height");

        field = new ConnectFourPlayer[width][height];
        takenRows = new int[width];
        this.height = height;
    }

    /**
     * Gets the width.
     *
     * @return the width. Will be greater than 0.
     */
    public int getWidth() {
        return field.length;
    }

    /**
     * Gets the height.
     *
     * @return the height. Will be greater than 0.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Checks whether the column <code>column</code> is full.
     *
     * @param column the column to check. Has to be in [0,
     * <code>getWidth() - 1</code>].
     * @return <code>true</code>, if no more coins can be inserted to the
     * column, <code>false</code> if there are still available cells.
     */
    public boolean isColumnFull(int column) {
        ArgumentExceptionHelper.ensureWithinRange(column, 0, getWidth() - 1, "column");

        return field[column][getHeight() - 1] != null;
    }

    /**
     * Gets the count of taken rows in column <code>column</code>.
     *
     * @param column the column. Has to be in [0, <code>getWidth() - 1</code>].
     * @return the count of taken rows in column <code>column</code>.
     */
    public int getCountOfTakenRows(int column) {
        ArgumentExceptionHelper.ensureWithinRange(column, 0, getWidth() - 1, "column");

        return takenRows[column];
    }

    /**
     * Gets the state of a cell.
     *
     * @param column the column. Has to be in [0, <code>getWidth() - 1</code>].
     * @param row the row. Has to be in [0, <code>getHeight() - 1</code>].
     * @return null, if the cell at (<code>column</code>, <code>row</code>) is
     * empty, or the player who owns the coin at that position.
     */
    public ConnectFourPlayer getState(int column, int row) {
        ArgumentExceptionHelper.ensureWithinRange(column, 0, getWidth() - 1, "column");
        ArgumentExceptionHelper.ensureWithinRange(row, 0, getHeight() - 1, "row");

        return field[column][row];
    }

    /**
     * Removes a coin from column <code>column</code>
     *
     * @param column the column to remove the coin from. Has to be in [0,
     * <code>getWidth() - 1</code>]. A {@link ConnectFourException} will be
     * thrown, if the column is empty.
     *
     * @return the player who owns the removed coin.
     */
    public ConnectFourPlayer removeCoinAndGetCoinOwner(int column) {
        ArgumentExceptionHelper.ensureWithinRange(column, 0, getWidth() - 1, "column");

        int highestRow = takenRows[column] - 1;
        if (highestRow == -1) {
            throw new ConnectFourException("The column is empty");
        }
        ConnectFourPlayer coinOwner = field[column][highestRow];
        field[column][highestRow] = null;
        takenRows[column]--;
        return coinOwner;
    }

    /**
     * Inserts a coin to column <code>column</code>.
     *
     * Throws a {@link ConnectFourException} if no more rows are available in
     * column <code>column</code>.
     *
     * @param column the column to insert the coin to. Has to be in [0,
     * <code>getWidth() - 1</code>].
     * @param coinOwner the player who owns the coin. Cannot be null.
     */
    public void insertCoin(int column, ConnectFourPlayer coinOwner) {
        ArgumentExceptionHelper.ensureWithinRange(column, 0, getWidth() - 1, "column");
        ArgumentExceptionHelper.ensureNotNull(coinOwner, "coinOwner");

        int rowCount = takenRows[column];
        if (rowCount == height) {
            throw new ConnectFourException("The column is full");
        }
        field[column][rowCount] = coinOwner;
        takenRows[column]++;
    }

    /**
     * Gets a string representation of this game field. The string
     * representation will be equal to <code>toString(null, null)</code>.
     *
     * @return the string representation. Will not be null.
     */
    @Override
    public String toString() {
        return toString(null, null);
    }

    /**
     * Gets a string representation of this game field. The field at
     * <code>getState(0, 0)</code> will be the bottom left cell in the string
     * representation, the field at
     * <code>getState(getWidth() - 1, getHeight() - 1)</code> the top right.
     * Cells are printed from left to right, rows from top to bottom. Each cell
     * is enclosed by "|", each row except the last will end with
     * <code>System.lineSeparator()</code>. A cell will contain a whitespace if
     * the field at that position is empty. If it is not empty and the player
     * who owns the coin at that position is equal to <code>firstPlayer</code>
     * or <code>secondPlayer</code>, the cell will contain "1" or "2", otherwise
     * the cell will contain the name of the player.
     *
     * @param firstPlayer the firstPlayer. Can be null.
     * @param secondPlayer the secondPlayer. Can be null.
     * @return the string representation. Will not be null.
     */
    public String toString(ConnectFourPlayer firstPlayer, ConnectFourPlayer secondPlayer) {
        StringBuilder sb = new StringBuilder();

        for (int row = getHeight() - 1; row >= 0; row--) {
            for (int column = 0; column < getWidth(); column++) {
                sb.append("|");
                ConnectFourPlayer player = getState(column, row);
                if (player == null) {
                    sb.append(" ");
                } else if (player == firstPlayer) {
                    sb.append("1");
                } else if (player == secondPlayer) {
                    sb.append("2");
                } else {
                    sb.append(player.getName());
                }
            }

            sb.append("|");
            if (row != 0) {
                sb.append(System.lineSeparator());
            }
        }

        return sb.toString();
    }
}
