package final1.de.hediet;

import final1.algorithm.Game;
import final1.algorithm.GameState;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The connect four game.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ConnectFourGame implements Game<ConnectFourMove> {

    /*
     * fieldWidth and fieldHeight cache the width and height of the game 
     * field, since they will be accessed a few million times.
     */
    private final int fieldWidth;
    private final int fieldHeight;
    private final ConnectFourField field;
    private final ConnectFourPlayer player1;
    private final ConnectFourPlayer player2;
    private ConnectFourPlayer currentPlayer;
    private GameState currentGameState = GameState.PLAYING;
    /**
     * The history of all moves. Undone moves are removed. ArrayDeque is a lot
     * faster than Stack. Will be empty after construction.
     */
    private final ArrayDeque<ConnectFourMove> moveHistory;
    /**
     * The history of all threats. Threats of undone moves are removed. Will
     * contain the empty set after construction.
     */
    private final ArrayDeque<Set<ConnectFourThreat>> threatHistory;
    /**
     * The weighted field used to sort the moves in {@link #getValidMoves()}.
     */
    private final int[][] weightedField;
    /**
     * The minimum weighted value, used in {@link #getValidMoves()}.
     */
    private final int minWeightedValue;
    /**
     * The maximum weighted value, used in {@link #getValidMoves()}.
     */
    private final int maxWeightedValue;

    /**
     * Creates a new connect four game. <code>player1</code> begins the game.
     * <code>player1</code> and <code>player2</code> have to be different instances.
     *
     * @param player1 the first player. Cannot be null.
     * @param player2 the second player. Cannot be null.
     * @param field the used field. Cannot be null.
     */
    public ConnectFourGame(ConnectFourPlayer player1, ConnectFourPlayer player2,
            ConnectFourField field) {
        ArgumentExceptionHelper.ensureNotNull(player1, "player1");
        ArgumentExceptionHelper.ensureNotNull(player2, "player2");
        ArgumentExceptionHelper.ensureNotNull(field, "field");

        if (player1 == player2) {
            throw new IllegalArgumentException(
                    "Arguments player1 and player2 cannot be the same instance!");
        }

        this.field = field;
        this.player1 = player1;
        this.player2 = player2;
        currentPlayer = player1;

        fieldWidth = field.getWidth();
        fieldHeight = field.getHeight();

        //set initial capacity to maximum moves (including initial values)
        moveHistory = new ArrayDeque<>(fieldWidth * fieldHeight);
        threatHistory = new ArrayDeque<>(fieldWidth * fieldHeight + 1);
        //this is to ensure threatHistory.peek() will always be defined.
        threatHistory.push(new HashSet<ConnectFourThreat>());

        weightedField = ConnectFourGameHelper.getNumberOfPossible4Lines(fieldWidth, fieldHeight);
        minWeightedValue = ConnectFourGameHelper.getMinValueIn(weightedField);
        maxWeightedValue = ConnectFourGameHelper.getMaxValueIn(weightedField);
    }

    /**
     * Gets the width of the field.
     *
     * @return the width of the field. Will be greater than 0.
     */
    public int getFieldWidth() {
        return fieldWidth;
    }

    /**
     * Gets the height of the field.
     *
     * @return the height of the field. Will be greater than 0.
     */
    public int getFieldHeight() {
        return fieldHeight;
    }

    /**
     * Gets the state of a cell.
     *
     * @param column the column. Has to be in
     * [0,<code>getFieldWidth() - 1</code>].
     * @param row the row. Has to be in [0,<code>getFieldHeight() - 1</code>].
     * @return null, if the cell at (<code>column</code>,<code>row</code>) is
     * empty, or the player who owns the coin at that position.
     */
    public ConnectFourPlayer getFieldState(int column, int row) {
        ArgumentExceptionHelper.ensureWithinRange(column, 0, fieldWidth - 1, "column");
        ArgumentExceptionHelper.ensureWithinRange(row, 0, fieldHeight - 1, "row");

        return field.getState(column, row);
    }

    /**
     * Gets a player by its position.
     *
     * @param position the one-based position of the player. Has to to be in [1,
     * 2].
     * @return the player. Will not be null.
     */
    public ConnectFourPlayer getPlayerByPosition(int position) {
        if (position == 1) {
            return player1;
        } else if (position == 2) {
            return player2;
        }
        throw new IllegalArgumentException(String.format(
                "Argument position has to be in [1, 2], but was %d.", position));
    }

    /**
     * Gets the position of a player.
     *
     * @param player the player. Cannot be null and has to participate in this
     * game.
     * @return the position of the player. 1, if <code>player</code> is the
     * first player, 2 if <code>player</code> is the second player.
     */
    public int getPlayerPosition(ConnectFourPlayer player) {
        ArgumentExceptionHelper.ensureNotNull(player, "player");

        if (player == player1) {
            return 1;
        } else if (player == player2) {
            return 2;
        } else {
            throw new IllegalArgumentException(String.format(
                    "Argument player '%s' does not participate in this game.", player));
        }
    }

    /**
     * Calculates and returns the list of possible moves among which the current
     * player can choose. If the game is over or no move is possible the list is
     * empty.
     *
     * This method has a runtime of O({@link #getFieldWidth}). The result of
     * profiling is that using {@link java.util.Collections#sort} is about 600ms
     * slower in contrast to this custom sort when two AI players with depth of
     * 11 are playing on a 7x6 field. An adaption of bucket sort is used.
     *
     * @return the list of possible moves.
     */
    @Override
    public List<ConnectFourMove> getValidMoves() {
        if (currentGameState != GameState.PLAYING) {
            //If the game is over or no move is possible the list is empty.
            return Collections.emptyList();
        }

        //threatedColumns is used to determine in O(1) whether a column is threated.
        boolean[] threatedColumns = new boolean[fieldWidth];
        for (ConnectFourThreat threat : threatHistory.peek()) {
            if (threat.getThreateningPlayer() == currentPlayer) {
                threatedColumns[threat.getColumn()] = true;
            }
        }

        //sortedMoveNodes will contain all valid moves,
        //indexed by maxWeightedValue minus weight of the field which will be taken if the move is performed.
        //The substraction of maxWeightedValue will lead to an inversed order.
        //If multiple moves are mapped to the same weight, the second move will be prepended to the first move.
        ConnectFourMoveBucket[] sortedMoveNodes = new ConnectFourMoveBucket[maxWeightedValue - minWeightedValue + 1];

        int validMoveCount = 0;
        int threatedColumnCount = 0;

        for (int column = fieldWidth - 1; column >= 0; column--) {
            if (field.isColumnFull(column)) {
                continue; //moves to full columns are not valid.
            }
            validMoveCount++;
            if (threatedColumns[column]) {
                threatedColumnCount++;
            }
            ConnectFourMove move = new ConnectFourMove(column, currentPlayer);

            int nextRow = field.getCountOfTakenRows(column);
            int index = maxWeightedValue - weightedField[column][nextRow];

            //Because the for-loop iterates descending from fieldWidth - 1 to 0, 
            //moves to lower columns are chained before moves to higher columns if they have the same weight.
            sortedMoveNodes[index] = new ConnectFourMoveBucket(move, sortedMoveNodes[index]);
        }

        ConnectFourMove[] result = new ConnectFourMove[validMoveCount];

        //Moves to threated columns have to be listed first, 
        //so the result is divided up into two spaces.
        //The first space goes from 0 to threatedColumnCount-1, 
        //the second from threatedColumnCount to validMoveCount-1.
        int threatedColumnsOffset = 0;
        int nonThreatedColumnsOffset = threatedColumnCount;

        //Because foreach is ascending, smaller indices come first.
        //Since index = maxWeightedValue - weightedField[column][nextRow],
        //a smaller index means a higher weight. Thus, higher weights are considered first.
        for (ConnectFourMoveBucket bucket : sortedMoveNodes) {
            //Moves to lower columns are chained before moves to higher columns.
            for (ConnectFourMoveBucket cur = bucket; cur != null; cur = cur.getNext()) {
                //depending whether the column of the move is threated, 
                //the move will be located within the first or the second space.
                ConnectFourMove move = cur.getMove();
                if (threatedColumns[move.getColumn()]) {
                    result[threatedColumnsOffset] = move;
                    threatedColumnsOffset++;
                } else {
                    result[nonThreatedColumnsOffset] = move;
                    nonThreatedColumnsOffset++;
                }
            }
        }

        assert threatedColumnsOffset == threatedColumnCount : "The first space has to be filled completely";
        assert nonThreatedColumnsOffset == validMoveCount : "The second space has to be filled completely";

        return Arrays.asList(result);
    }

    /**
     * Ensures that a move is valid, so it can be performed. If it is not, a
     * ConnectFourException will be thrown. A move is invalid, if either column
     * is not in <code>[0,getFieldWidth() - 1]</code> or the column is full.
     *
     * A {@link ConnectFourException} will be thrown, if the move is invalid.
     *
     * @param move the move to check. Cannot be null.
     */
    public void ensureValidMove(ConnectFourMove move) {
        ArgumentExceptionHelper.ensureNotNull(move, "move");

        if (move.getColumn() < 0 || move.getColumn() >= fieldWidth) {
            throw new ConnectFourException(String.format(
                    "Invalid column, column has to be between 0 and %d, but was %d.",
                    getFieldWidth() - 1, move.getColumn()));
        }
        if (field.isColumnFull(move.getColumn())) {
            throw new ConnectFourException(String.format(
                    "Invalid move, column %d is full.", move.getColumn()));
        }
    }

    /**
     * Locally evaluates the current state of the game with respect to the
     * player who has the turn.
     *
     * @return a local evaluation of the current game state according to the
     * exercise sheet.
     */
    @Override
    public double evaluateState() {
        switch (getState()) {
            case DRAW:
                return 0;
            case PLAYER_1_WON:
                return currentPlayer == player1
                        ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
            case PLAYER_2_WON:
                return currentPlayer == player2
                        ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
            case PLAYING:
                double playerDifference = 0;

                for (ConnectFourThreat threat : threatHistory.peek()) {
                    //E.g. if threat.getRow() is 0 and the column is empty (i.e. taken rows is 0), r is 1.
                    int r = threat.getRow() - field.getCountOfTakenRows(threat.getColumn()) + 1;

                    //Math.log(1+r) is used instead of Math.log1p(r) due to the praktomat tests.
                    double value = 1.0 / Math.log(1.0 + r);
                    if (threat.getThreateningPlayer() == currentPlayer) {
                        playerDifference += value;
                    } else {
                        playerDifference -= value;
                    }
                }

                return playerDifference;

            default:
                throw new AssertionError("Game state not handled.");
        }
    }

    /**
     * Gets the next game state and collect all threats. At this point,
     * threatsHistory does not contain newThreats.
     */
    private GameState getNewGameStateAndCollectThreats(
            ConnectFourMove move, Set<ConnectFourThreat> newThreats) {
        assert move != null;
        assert newThreats != null;

        int moveRow = field.getCountOfTakenRows(move.getColumn()) - 1;
        int moveColumn = move.getColumn();
        ConnectFourPlayer player = move.getPlayer();

        //Copy threats from previuos moves.
        for (ConnectFourThreat oldThreat : threatHistory.peek()) {
            if (oldThreat.getColumn() != moveColumn || oldThreat.getRow() != moveRow) {
                //Threat was not blocked by 'move'.
                newThreats.add(oldThreat);
            } else if (oldThreat.getThreateningPlayer() == player) {
                //Threat was realized by 'move' - 'player' wins.
                return player == player1 ? GameState.PLAYER_1_WON : GameState.PLAYER_2_WON;
            }
        }

        ConnectFourGameHelper.detectThreatsCausedBy(player, field, moveColumn, moveRow, 1, 0, newThreats);
        ConnectFourGameHelper.detectThreatsCausedBy(player, field, moveColumn, moveRow, 1, 1, newThreats);
        ConnectFourGameHelper.detectThreatsCausedBy(player, field, moveColumn, moveRow, 0, 1, newThreats);
        ConnectFourGameHelper.detectThreatsCausedBy(player, field, moveColumn, moveRow, -1, 1, newThreats);

        if (moveHistory.size() == fieldWidth * fieldHeight) {
            return GameState.DRAW;
        }

        return GameState.PLAYING;
    }

    /**
     * Performs the given move for the player who has the turn.
     *
     * A {@link ConnectFourException} will be thrown, if the game is over, the
     * player, who owns the move, is not at turn, or the move is invalid (see
     * {@link #ensureValidMove}).
     *
     * @param move The move to perform. Cannot be null.
     */
    @Override
    public void perform(ConnectFourMove move) {
        ArgumentExceptionHelper.ensureNotNull(move, "move");
        if (currentGameState != GameState.PLAYING) {
            throw new ConnectFourException("Game is over.");
        }
        if (move.getPlayer() != currentPlayer) {
            throw new ConnectFourException(
                    String.format("The player %s is not at turn.", move.getPlayer()));
        }
        ensureValidMove(move);

        field.insertCoin(move.getColumn(), move.getPlayer());
        moveHistory.push(move);
        Set<ConnectFourThreat> newThreats = new HashSet<>();
        currentGameState = getNewGameStateAndCollectThreats(move, newThreats);
        threatHistory.push(newThreats);

        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    /**
     * Restores the game state before the last turn.
     *
     * A {@link ConnectFourException} will be thrown, if there is no move which
     * could be undone.
     */
    @Override
    public void undo() {
        if (moveHistory.isEmpty()) {
            throw new ConnectFourException("There is no move which could be undone.");
        }
        ConnectFourMove move = moveHistory.pop();
        threatHistory.pop();
        currentPlayer = move.getPlayer();
        currentGameState = GameState.PLAYING;

        ConnectFourPlayer player = field.removeCoinAndGetCoinOwner(move.getColumn());
        assert player == move.getPlayer();
    }

    /**
     * Returns the next move. If the player who has the turn is human the next
     * move will be asked for via the user-interface. If the player is
     * artificial the next move will be calculated by the AI algorithm. A
     * subsequent call to {@link #perform} is needed to execute the move.
     *
     * @return the next move to perform. Will not be null.
     */
    @Override
    public ConnectFourMove nextTurn() {
        ConnectFourMove move = currentPlayer.getNextMove();

        if (move == null) {
            throw new ConnectFourException(String.format(
                    "The move returned by %s cannot be null.", currentPlayer));
        }

        return move;
    }

    /**
     * Returns the game-state.
     *
     * @return one of the four possible game-states. Will not be null.
     */
    @Override
    public GameState getState() {
        assert currentGameState != null;
        return currentGameState;
    }

    /**
     * Gets a string representation of the field. The field will have the same
     * format as returned by {@link ConnectFourField#toString(ConnectFourPlayer, ConnectFourPlayer)
     * }.
     *
     * @return the string representation. Will not be null.
     */
    public String getFieldString() {
        return field.toString(player1, player2);
    }

    /**
     * Gets a string representation of this game, which contains the current
     * field, all valid moves, the current threats, the active player and the
     * current evaluation.
     *
     * @return the string representation. Will not be null.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getFieldString());

        for (ConnectFourMove m : getValidMoves()) {
            sb.append(System.lineSeparator());
            sb.append("Valid Move: ");
            sb.append(m);
        }

        for (ConnectFourThreat t : threatHistory.peek()) {
            sb.append(System.lineSeparator());
            sb.append(t);
        }

        sb.append(System.lineSeparator());
        sb.append("Current Player: ");
        sb.append(currentPlayer);

        sb.append(System.lineSeparator());
        sb.append("Evaluation: ");
        sb.append(evaluateState());

        return sb.toString();
    }
}
