package final1.de.hediet;

import java.util.Set;

/**
 * An helper class for {@link ConnectFourGame}. This class externalizes some
 * static methods to prevent exceeding the maximum line count per file.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
final class ConnectFourGameHelper {

    private ConnectFourGameHelper() {
        throw new AssertionError("Utility Classes must not be instantiated!");
    }

    /**
     * Gets the maximum value in <code>array</code>.
     *
     * @param array the array to search for the maximum value.
     * @return the maximum value.
     */
    public static int getMaxValueIn(int[][] array) {
        int max = Integer.MIN_VALUE;
        for (int[] subArray : array) {
            for (int j = 0; j < subArray.length; j++) {
                if (subArray[j] > max) {
                    max = subArray[j];
                }
            }
        }
        return max;
    }

    /**
     * Gets the minimum value in <code>array</code>.
     *
     * @param array the array to search for the minimum value.
     * @return the minimum value.
     */
    public static int getMinValueIn(int[][] array) {
        int min = Integer.MAX_VALUE;
        for (int[] subArray : array) {
            for (int j = 0; j < subArray.length; j++) {
                if (subArray[j] < min) {
                    min = subArray[j];
                }
            }
        }
        return min;
    }

    /**
     * Calculates hitsBeforeEmptyField, hitsAfterEmptyField, emptyFieldColumn
     * and emptyFieldRow. startX, startY, dX, dY describe a line from
     * <code>(startX, startY)</code> to
     * <code>(startX + i * dX, startY + i * dY)</code> where i is between 0 and
     * 3. i is chosen so that maximum one empty field is enclosed by the line
     * and all other enclosed fields are owned by <code>player</code>.
     * hitsBeforeEmptyField/hitsAfterEmptyField are the number of coins owned by
     * the player which are enclosed by the line and before/after the empty
     * field. emptyFieldColumn and emptyFieldRow describe the position of the
     * empty field. If emptyFieldColumn == -1, there is no empty field enclosed
     * by the line. This implies that hitsAfterEmptyField == 0.
     *
     * If two AI players play with depth of 11 on a 7x6 field, this method will
     * be called around 70.000.000 times.
     */
    private static CheckLineResult checkLine(ConnectFourPlayer player, ConnectFourField field,
            int startX, int startY, int dX, int dY) {
        assert player != null;
        assert dX != 0 || dY != 0;

        CheckLineResult result = new CheckLineResult();

        result.emptyFieldColumn = -1;
        result.emptyFieldRow = -1;

        int width = field.getWidth();
        int height = field.getHeight();
        boolean emptyFieldDetected = false;

        for (int i = 0, column = startX, row = startY;
                i < 4 && column >= 0 && column < width && row >= 0 && row < height;
                i++, column += dX, row += dY) {

            ConnectFourPlayer playerOnField = field.getState(column, row);

            if (playerOnField == null && !emptyFieldDetected) {
                //the first empty field was detected
                result.emptyFieldColumn = column;
                result.emptyFieldRow = row;
                emptyFieldDetected = true;
            } else if (playerOnField != player) {
                //either the second empty field or a coin of the opponent was detected.
                break;
            } else if (emptyFieldDetected) {
                result.hitsAfterEmptyField++;
            } else {
                result.hitsBeforeEmptyField++;
            }
        }

        return result;
    }

    /**
     * Detect all threats which are caused by the coin on position
     * <code>(originColumn, originRow)</code> =: 'origin' and are in the line
     * (originColumn + z * dX, originRow + z * dY), z being an integer.
     *
     * The coin on 'origin' has to be owned by <code>player</code>. Either dX or
     * dY has to be different from 0.
     *
     * If two AI players play with depth of 11 on a 7x6 field, this method will
     * be called around 35.000.000 times.
     *
     * @param player the player who owns the coin. Cannot be null.
     * @param field the field. Cannot be null.
     * @param originColumn the origin column. Has to be within
     * <code>[0, field.getWidth() - 1]</code>.
     * @param originRow the origin row. Has to be within
     * <code>[0, field.getHeight() - 1]</code>.
     * @param dX the delta x offset.
     * @param dY the delta y offset.
     * @param threats the set of threats which will be filled. Cannot be null.
     */
    static void detectThreatsCausedBy(ConnectFourPlayer player, ConnectFourField field,
            int originColumn, int originRow, int dX, int dY, Set<ConnectFourThreat> threats) {
        ArgumentExceptionHelper.ensureNotNull(player, "player");
        ArgumentExceptionHelper.ensureNotNull(field, "field");
        ArgumentExceptionHelper.ensureNotNull(threats, "threats");

        if (field.getState(originColumn, originRow) != player) {
            throw new IllegalArgumentException(String.format(
                    "The coin on (%d, %d) has to be owned by %s", originColumn, originRow, player));
        }

        if (dX == 0 && dY == 0) {
            throw new IllegalArgumentException("Either dX or dY has to be different from 0.");
        }

        CheckLineResult right = checkLine(player, field, originColumn, originRow, dX, dY);
        CheckLineResult left = checkLine(player, field, originColumn, originRow, -dX, -dY);

        //totalHitsBeforeEmptyField is the number of coins owned by the player around 'origin' (including 'origin').
        //Minus one, because the coin on 'origin' is count twice.
        int totalHitsBeforeEmptyField = left.hitsBeforeEmptyField + right.hitsBeforeEmptyField - 1;

        if (left.emptyFieldColumn != -1 && left.hitsAfterEmptyField + totalHitsBeforeEmptyField >= 3) {
            threats.add(new ConnectFourThreat(left.emptyFieldColumn, left.emptyFieldRow, player));
        }
        if (right.emptyFieldColumn != -1 && right.hitsAfterEmptyField + totalHitsBeforeEmptyField >= 3) {
            threats.add(new ConnectFourThreat(right.emptyFieldColumn, right.emptyFieldRow, player));
        }
    }

    /**
     * This method calculates for each cell the number of possible sequences
     * which contains four coins. Vertical, horizontal and diagonal sequences
     * are considered.
     *
     * @param width the width of the field. Has to be greater than or equal to
     * 0.
     * @param height the height of the field. Has to be greater than or equal to
     * 0.
     * @return a two dimensional array which contains the number of possible
     * 4-sequences of each cell. The first dimension is <code>width</code>, the
     * second dimension is <code>height</code>.
     */
    static int[][] getNumberOfPossible4Lines(int width, int height) {
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(width, 0, "width");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(height, 0, "height");

        int[][] result = new int[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                //free fields around (i,j) in each direction.
                //the field on (i,j) is not considered.
                int freeFieldsOnRight = Math.min(width - i - 1, 3);
                int freeFieldsOnLeft = Math.min(i, 3);
                int freeFieldsOnBottom = Math.min(height - j - 1, 3);
                int freeFieldsOnTop = Math.min(j, 3);
                int freeFieldsOnRightBottom = Math.min(freeFieldsOnRight, freeFieldsOnBottom);
                int freeFieldsOnLeftTop = Math.min(freeFieldsOnLeft, freeFieldsOnTop);
                int freeFieldsOnRightTop = Math.min(freeFieldsOnRight, freeFieldsOnTop);
                int freeFieldsOnLeftBottom = Math.min(freeFieldsOnLeft, freeFieldsOnBottom);

                //plus 1, because the field on (i,j) is not considered.
                //minus 3, because 4 fields are enough to get 1 combination.
                int horizontalCombinations = Math.max(0, 1 + freeFieldsOnRight + freeFieldsOnLeft - 3);
                int verticalCombinations = Math.max(0, 1 + freeFieldsOnBottom + freeFieldsOnTop - 3);
                int diagonal1Combinations = Math.max(0, 1 + freeFieldsOnRightBottom + freeFieldsOnLeftTop - 3);
                int diagonal2Combinations = Math.max(0, 1 + freeFieldsOnRightTop + freeFieldsOnLeftBottom - 3);

                result[i][j] = horizontalCombinations + verticalCombinations
                        + diagonal1Combinations + diagonal2Combinations;
            }
        }

        return result;
    }

    /**
     * Used in {@link #checkLine} to return all values. This class is used to
     * overcome the limit of 1 return value. The private fields are accessed
     * directly.
     */
    private static class CheckLineResult {

        private int hitsBeforeEmptyField;
        private int hitsAfterEmptyField;
        private int emptyFieldColumn;
        private int emptyFieldRow;

        public CheckLineResult() {
        }
    }
}
