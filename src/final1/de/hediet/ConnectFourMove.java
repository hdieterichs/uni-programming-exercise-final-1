package final1.de.hediet;

/**
 * A connect four move, consisting of a player and a column. When performed, a
 * new coin owned by {@link #getPlayer} is inserted into {@link #getColumn}.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ConnectFourMove implements final1.algorithm.Move {

    private final ConnectFourPlayer player;
    private final int column;

    /**
     * Creates a new move.
     *
     * @param column the column.
     * @param player the player. Cannot be null.
     */
    public ConnectFourMove(int column, ConnectFourPlayer player) {
        ArgumentExceptionHelper.ensureNotNull(player, "player");

        this.column = column;
        this.player = player;
    }

    /**
     * Gets the player who owns the move.
     *
     * @return the player. Will not be null.
     */
    public ConnectFourPlayer getPlayer() {
        return player;
    }

    /**
     * Gets the column the coin will be inserted into.
     *
     * @return the column.
     */
    public int getColumn() {
        return column;
    }

    /**
     * Gets a string representation of this object.
     *
     * @return the string representation. Will not be null.
     */
    @Override
    public String toString() {
        return String.format("Move from %s to column %d", getPlayer(), getColumn());
    }
}
