package final1.de.hediet;

/**
 * Class used for sorting moves. Each bucket contains one move and may refer to
 * another bucket, so that buckets can be chained. This is inspired by the
 * concept of linked lists.
 *
 * @author Henning Dieterichs
 * @version 1.0
 * @see ConnectFourGame#getValidMoves()
 */
class ConnectFourMoveBucket {

    private final ConnectFourMoveBucket next;
    private final ConnectFourMove move;

    /**
     * Creates a new bucket which contains a move.
     *
     * @param move the move. Cannot be null.
     * @param next the bucket which contains the next move. Can be null.
     */
    public ConnectFourMoveBucket(ConnectFourMove move, ConnectFourMoveBucket next) {
        ArgumentExceptionHelper.ensureNotNull(move, "move");

        this.move = move;
        this.next = next;
    }

    /**
     * Gets the bucket which contains the next move.
     *
     * @return the bucket which contains the next move. Can be null.
     */
    public ConnectFourMoveBucket getNext() {
        return next;
    }

    /**
     * Gets the move inside this bucket.
     *
     * @return the move inside this bucket. Will not be null.
     */
    public ConnectFourMove getMove() {
        return move;
    }
}
