package final1.de.hediet;

/**
 * The base class for a player.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public abstract class ConnectFourPlayer {

    private final String name;
    private ConnectFourGame game;

    /**
     * Creates a new player.
     *
     * @param name the name. Cannot be null.
     */
    protected ConnectFourPlayer(String name) {
        ArgumentExceptionHelper.ensureNotNull(name, "name");
        this.name = name;
    }

    /**
     * Gets the name of the player.
     *
     * @return the name. Will not be null.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the game. This method will be called after the game was created.
     * This method cannot be called more than once.
     *
     * @param game the game. Cannot be null.
     */
    public void setGame(ConnectFourGame game) {
        ArgumentExceptionHelper.ensureNotNull(game, "game");

        if (this.game != null) {
            throw new AssertionError("Game already set.");
        }

        this.game = game;
    }

    /**
     * Gets the game. The game has to be set before by {@link #setGame}.
     *
     * @return the game. Will not be null.
     */
    protected ConnectFourGame getGame() {
        if (game == null) {
            throw new AssertionError("Game not set.");
        }

        return game;
    }

    /**
     * Gets the next move.
     *
     * @return the next move. Will not be null.
     */
    public abstract ConnectFourMove getNextMove();

    /**
     * Gets a string representation of this player.
     *
     * @return the string representation. Will not be null.
     */
    @Override
    public String toString() {
        return getName();
    }
}
