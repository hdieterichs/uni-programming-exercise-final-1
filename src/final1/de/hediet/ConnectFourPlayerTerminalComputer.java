package final1.de.hediet;

import final1.Terminal;
import final1.algorithm.GameException;

/**
 * A computer player which prints the steps to {@link Terminal}.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ConnectFourPlayerTerminalComputer extends ConnectFourPlayer {

    private AlphaBetaAlgorithm<ConnectFourGame, ConnectFourMove> algorithm;
    private final int depth;

    /**
     * Creates a new computer player.
     *
     * @param name the name of the player. Cannot be null.
     * @param depth the search depth of the used {@link AlphaBetaAlgorithm}. Has
     * to be greater than or equal to 1.
     */
    public ConnectFourPlayerTerminalComputer(String name, int depth) {
        super(name);
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(depth, 1, "depth");
        
        this.depth = depth;
    }

    /**
     * Gets the next move. The move is calculated by using an
     * AlphaBetaAlgorithm.
     *
     * After the move was calculated, the column will be printed to
     * {@link Terminal}, according to requirements of the exercise sheet.
     *
     * @return the next move. Will not be null.
     */
    @Override
    public ConnectFourMove getNextMove() {
        if (algorithm == null) {
            algorithm = new AlphaBetaAlgorithm<>(getGame(), depth);
        }
        try {
            ConnectFourMove move = algorithm.getBestMove();

            Terminal.prompt(getName() + ": ");
            Terminal.println(Integer.toString(move.getColumn()));
            
            return move;
        } catch (GameException e) {
            throw new ConnectFourException("The AI algorithm has thrown a game exception.", e);
        }
    }
}
