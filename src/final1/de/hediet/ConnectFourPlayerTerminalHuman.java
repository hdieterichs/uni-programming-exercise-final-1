package final1.de.hediet;

import final1.Terminal;

/**
 * A human player which uses {@link Terminal} to ask for the next move.
 * 
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ConnectFourPlayerTerminalHuman extends ConnectFourPlayer {

    /**
     * Creates a new human player.
     * 
     * @param name the name of the player. Cannot be null.
     */
    public ConnectFourPlayerTerminalHuman(String name) {
        super(name);
    }

    /**
     * Gets the next move.
     * The next move will be asked from the user by using {@link Terminal}. 
     * 
     * @return the next move. Will not be null.
     */
    @Override
    public ConnectFourMove getNextMove() {
        while (true) {
            try {
                Terminal.prompt(getName() + ": ");
                String columnStr = Terminal.readln();
                int column = Integer.parseInt(columnStr);
                ConnectFourMove move = new ConnectFourMove(column, this);
                getGame().ensureValidMove(move);
                
                return move;

            } catch (NumberFormatException e) {
                Terminal.println(ConnectFour.ERROR_PREFIX 
                        + "the input has to be a valid number.");
            } catch (ConnectFourException e) {
                Terminal.println(ConnectFour.ERROR_PREFIX 
                        + "the move is invalid: " + e.getMessage());
            }
        }
    }

}
