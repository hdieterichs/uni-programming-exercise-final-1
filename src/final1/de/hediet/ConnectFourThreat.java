package final1.de.hediet;

/**
 * A threat, consisting of a player, a column and a row. A threat is an empty
 * field on <code>(column, row)</code>, which will lead to a win if
 * <code>threateningPlayer</code> inserts a coin to <code>column</code> so that
 * the coin will fall to <code>row</code>.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
class ConnectFourThreat {

    private final int column;
    private final int row;
    private final ConnectFourPlayer threateningPlayer;

    /**
     * Creates a new threat.
     *
     * @param column the column. Has to be greater than or equal to 0.
     * @param row the row. Has to be greater than or equal to 0.
     * @param threateningPlayer the player who caused this threat. Cannot be
     * null.
     */
    public ConnectFourThreat(int column, int row, ConnectFourPlayer threateningPlayer) {
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(column, 0, "column");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(row, 0, "row");
        ArgumentExceptionHelper.ensureNotNull(threateningPlayer, "threateningPlayer");

        this.column = column;
        this.row = row;
        this.threateningPlayer = threateningPlayer;
    }

    /**
     * Gets the column.
     *
     * @return the column. Will be greater than or equal to 0.
     */
    public int getColumn() {
        return column;
    }

    /**
     * Gets the row.
     *
     * @return the row. Will be greater than or equal to 0.
     */
    public int getRow() {
        return row;
    }

    /**
     * Gets the player who caused the threat.
     *
     * @return the player. Will not be null.
     */
    public ConnectFourPlayer getThreateningPlayer() {
        return threateningPlayer;
    }

    /**
     * Gets the hashcode of the threat. Two threats which are equal will have
     * the same hashcode.
     *
     * @return the hashcode.
     */
    @Override
    public int hashCode() {
        int hash = 19;
        hash = hash * 31 + column;
        hash = hash * 31 + row;
        hash = hash * 31 + threateningPlayer.hashCode();
        return hash;
    }

    /**
     * Checks whether <code>obj</code> is equal to <code>this</code>. Two
     * threats are equal, if each {@link #getColumn}, {@link #getRow} and
     * {@link #getThreateningPlayer} are matching. The threatening player has to
     * be the same object.
     *
     * @param obj the object to compare.
     * @return <code>true</code>, if they are equal, otherwise
     * <code>false</code>.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!getClass().equals(obj.getClass())) {
            return false;
        }
        ConnectFourThreat other = (ConnectFourThreat) obj;
        return this.column == other.column
                && this.row == other.row
                && this.threateningPlayer == other.threateningPlayer;
    }

    /**
     * Gets a string representation of this threat.
     *
     * @return the string representation. Will not be null.
     */
    @Override
    public String toString() {
        return String.format("Threat on column %d in row %d, caused by %s",
                getColumn(), getRow(), getThreateningPlayer());
    }
}
