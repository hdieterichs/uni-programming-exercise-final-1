package final1.de.hediet;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 * GameHelperTest
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ConnectFourGameHelperNGTest {

    /**
     * The default constructor.
     */
    public ConnectFourGameHelperNGTest() {
    }
    
    /**
     * Test of getNumberOfPossible4Lines method, of class ConnectFourGameHelper.
     */
    @Test
    public void testGetNumberOfPossible4Lines() {
        int[][] expResult = {{3, 4, 5, 5, 4, 3},
        {4, 6, 8, 8, 6, 4},
        {5, 8, 11, 11, 8, 5},
        {7, 10, 13, 13, 10, 7},
        {5, 8, 11, 11, 8, 5},
        {4, 6, 8, 8, 6, 4},
        {3, 4, 5, 5, 4, 3}};

        int[][] result = ConnectFourGameHelper.getNumberOfPossible4Lines(7, 6);
        assertEquals(result, expResult);
    }

}
